package com.gilbertcorp.market.controller;

import com.gilbertcorp.market.service.MarketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 */
@RestController
public class MarketController {

    private final MarketService marketService;

    @Autowired
    public MarketController(MarketService marketService) {
        this.marketService = marketService;
    }

    @RequestMapping("createOffer")
    public HttpStatus createOffer() {
        return HttpStatus.OK;
    }
}
